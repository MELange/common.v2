﻿// <copyright file="LocalizedString.cs" company="Monosoft Holding ApS">
// Copyright 2018 Monosoft Holding ApS
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
using System.Collections.Generic;

namespace ITUtil.Common.Base
{
    /// <summary>
    /// Localized string definition
    /// </summary>
    public partial class LocalizedString
    {
        public LocalizedString()
        {
        }

        public LocalizedString(string lang, string text)
        {
            this.lang = lang;
            this.text = text;
        }

        public static LocalizedString Create(string lang, string text)
        {
            return new LocalizedString(lang, text);
        }

    

        /// <summary>
        /// Gets or sets Lang
        /// </summary>
        public string lang { get; set; }

        /// <summary>
        /// Gets or sets Text
        /// </summary>
        public string text { get; set; }
    }
}