﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITUtil.Common.Base
{
    public partial class LocalizedString
    {
        public static LocalizedString[] OK
        {
            get
            {
                return new List<LocalizedString>() { LocalizedString.Create("en", "OK"), LocalizedString.Create("dk", "OK") }.ToArray();
            }
        }

        public static LocalizedString[] Error
        {
            get
            {
                return new List<LocalizedString>() { LocalizedString.Create("en", "Error"), LocalizedString.Create("dk", "Error") }.ToArray();
            }
        }

        public static LocalizedString[] UnknownOperation
        {
            get
            {
                return new List<LocalizedString>() { LocalizedString.Create("en", "Unknown operation"), LocalizedString.Create("dk", "Ukendt operation") }.ToArray();
            }
        }

        public static LocalizedString[] MissingCredentials
        {
            get
            {
                return new List<LocalizedString>() { LocalizedString.Create("en", "Missing credentials"), LocalizedString.Create("dk", "Manglende rettighed") }.ToArray();
            }
        }
    }
}
