﻿namespace ITUtil.Command
{
    using System.Collections.Generic;

    public interface INamespace
    {
        public string Name { get; }
        public List<ICommandBase> Commands { get; set; }
        ITUtil.Common.DTO.ProgramVersion ProgramVersion { get; }
    }
}
