﻿namespace ITUtil.Command
{
    public interface ICommandBase 
    {
        public string Description { get; }

        public Claim Claims { get; }
    }
}
