﻿using ITUtil.Common.Base;

namespace ITUtil.Command
{
    public partial class Claim
    {
        /// <summary>
        /// User is administrator, i.e. can see all orders.
        /// </summary>
        public static Claim IsAdmin = new Claim() 
        { 
            Key = "IsAdmin", 
            dataContext = DataContextEnum.organisationClaims, 
            description = new[] 
            { 
                new LocalizedString("en", "User is administrator, i.e. can see all orders"),
                new LocalizedString("da", "bruger er adminstrator,så de kan f.eks se alle ordre."), 
            }, 
        };
    }
}