﻿using ITUtil.Common.Base;
using System;

namespace ITUtil.Command
{
    public partial class Claim
    {

        public enum DataContextEnum
        {
            /// <summary>
            /// Claims that the user have given to the organisation, such as "can edit my profile", "allowed to send me a newsletter" etc.
            /// </summary>
            userClaims,
            /// <summary>
            /// Claims that the organisation have given to a user, suchs as "is database administrator"
            /// </summary>
            organisationClaims,
        }

        public string Key { get; set; }

        public LocalizedString[] description { get; set; }

        public DataContextEnum dataContext { get; set; }
    }
}