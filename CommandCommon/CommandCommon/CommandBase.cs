﻿namespace ITUtil.Command
{
    using ITUtil.Common.DTO;

    public abstract class CommandBase<T, U> : ICommand<T, U> where T : class where U : class
    {
        public abstract string Description { get; }

        public abstract Claim Claims { get; }

        public abstract U Execute(T input);

        public CommandBase(INamespace inamespace)
        {
            inamespace.Commands.Add(this);
        }
    }
}
