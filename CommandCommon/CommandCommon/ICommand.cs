﻿namespace ITUtil.Command
{
    public interface ICommand <T,U>: ICommandBase where T : class where U : class
    {
        public U Execute(T input);
    }
}
