﻿namespace ITUtil.Config
{
    public class RedisSettings : Config
    {
        public string IP { get; set; }
        public int Port { get; set; }
        public override string ToString()
        {
            return $"{this.IP}:{this.Port}";
        }
    }

}
