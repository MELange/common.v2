﻿namespace ITUtil.Config
{
    using System.Collections.Generic;
    using System.IO;
    public class GlobalRessources
    {
      
        public Dictionary<System.Type, Config> Configs { get; private set; }

        public static GlobalRessources getConfig(string settingsFilename = null)
        {
            string filename = Directory.GetCurrentDirectory() + "/" + (string.IsNullOrEmpty(settingsFilename) ? "appsettings.json" : settingsFilename);
            
            if (File.Exists(filename))
            {
                string json = File.ReadAllText(filename);
                var globalResources = Newtonsoft.Json.JsonConvert.DeserializeObject<GlobalRessources>(json);
                
                return globalResources;
            }
            
            return null;
        }

        /// <summary>
        /// please use ITUtil.Config.Implementation
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T GetSetting<T>()where T: Config
        {
            return (T)this.Configs[typeof(T)];
        }



    }
}
