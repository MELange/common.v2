﻿namespace ITUtil.Config
{
    public class StorageSettings : Config
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Uri { get; set; }
    }

}
