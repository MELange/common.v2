﻿using ITUtil.Command;
using ITUtil.Common.DTO;
using System;
using System.Collections.Generic;

namespace Dibs.V1
{
    public class PaymentController : INamespace
    {
        public string Name { get; private set; }

        public List<ICommandBase> Commands { get; set; }

        public ProgramVersion ProgramVersion { get; private set; }

        public PaymentController()
        {
            this.Commands = new List<ICommandBase>();
            this.ProgramVersion = new ProgramVersion(1, 0, 0, 0);
            this.Name = "DIbsPayment";
        }

    }
}
