﻿using ITUtil.Command;
using System;
using V1.DTO;

namespace Dibs.V1
{
    public class SetSettings : CommandBase<Settings, object>
    {
        public SetSettings(INamespace inamespace) : base(inamespace)
        {
            this.Claims = Claim.IsAdmin;
            this.Description = "Set the settings for this micro service, "+ inamespace.Name +".";
        }

        public override string Description { get; }

        public override Claim Claims { get; }

        public override object Execute(Settings input)
        {
            return null;
        }
    }
}
